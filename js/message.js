// the message box at the lower right of the screen
// (C) Jacques Mattheij 2020; 2021; 2022; 2023; jacques@modularcompany.com
// All rights reserved, 

function message_user(m)

{
        SimpleToast.info(m);
}

// Simple Toast class that allows for warning and info modes to have different classes.
// Usage: SimpleToast.warn("Message") or SimpleToast.info("Message")

let SimpleToast = (function() {

        info = function(m) {
                showToast(m, "toastinfo");
        };

        warn = function(m) {
                showToast(m, "toastwarn");

        };

        let showToast = function(m, css) {

                $("#info").text(m);
                $("#info").attr('class', css);
                $("#info").fadeIn('fast');

                // remove the info box again after the user has had a chance to read it

                window.setTimeout(function () {
                        $("#info").fadeOut('fast');
                }, 5000);
        };
        return self;

})();

