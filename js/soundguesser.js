
// this file governs the soundguesser tab
// it is completely 'blind', there are no visual elements to this training
// the computer plays a note or a chord relative to the previous note or 
// chord and you then have to guess what it is
// first failure is all that counts. A level completed perfectly
// allows you to level up.

// when you make a mistake the note
// or chord is repeated. 

// - add multi-note sequences; turn all levels into sequences of one or more notes
// - add harmonic / melodic
// - add real pieces of music
// - add a time bonus
// x add level chooser
// - add level log and a graph showing progress over time
// - praise for achieving personal best scores

function gp_fill_rect(c, x, y , w, h , color)

{
        c.fillStyle = color;
        c.fillRect(x,y,w,h);
}

// draw a single soundguesser score
// maximum score is always 10000, 100% good and zero speed penalty
// 'good' score is 5000, penalty is the other 5000

function sg_draw_stat(c, i, h)

{
        gp_fill_rect(c, i-1, h-1, 3, 3, "red");
}

function sg_draw_stats(stats)

{
        var sgstatsrect = document.getElementById("sgstats").getBoundingClientRect();
        $("#sgstats").attr('width', sgstatsrect.width);
        $("#sgstats").attr('height', sgstatsrect.height);
        var c = document.getElementById("sgstats").getContext("2d");

        // wipe out the rectangle

        gp_fill_rect(c,0,0,sgstatsrect.width, sgstatsrect.height, "white");

        for (var i=0;i<sgstatsrect.width;i++) {
                if (i < stats.length) {
                        sg_draw_stat(c, i, Math.floor(((10000-stats[i].points) / 10000) * sgstatsrect.height));
                }
        }
}

function percentage_format(p)

{
        p = p * 10000;

        p = Math.floor(p) / 10000;

        return p;
}

function sg_user_score()

{
        if ((sg_session_hits + sg_session_misses) == 0) {
                sg_session_score = 0;
        }
        else {
                sg_session_score = 100 * (sg_session_hits / (sg_session_hits + sg_session_misses));
        }

        sg_session_score = percentage_format(sg_session_score);

        if ((sg_level_hits + sg_level_misses) == 0) {
                sg_level_score = 0;
        }
        else {
                sg_level_score = 100 * (sg_level_hits / (sg_level_hits + sg_level_misses));
        }

        // max 4500 points if all 30 are right on time

        sg_level_score = 150 * sg_level_hits;

        sg_level_endtick = Date.now();

        sg_level_took = (sg_level_endtick - sg_level_starttick) / 1000;

        console.log("took: " + sg_level_took);

        if (sg_level_misses == 0) {
                // no misses, no calculate the time bonus
                // 300 seconds is the baseline
                // for everything faster than that
            
                console.log("Perfect!");

                // add up to 5500 points for perfect score and zero seconds (nobody will ever get that
                // but that's fine).

                if (sg_level_took < 120) {
                        console.log("have a time bonus " + ((120 - sg_level_took) * 45.833));
                        sg_level_score += Math.floor((45.833 * (120 - sg_level_took)));
                }
        }
}

var sg_level = 0;
var sg_levels = [ 
        [12],
        [1],
        [2],
        [1,2],
        [1, 2, 12],
        [3],
        [3, 12],
        [4], 
        [4, 12], 
        [3,4,12], 
        [1, 3, 12], 
        [1, 4, 12], 
        [1, 3,4,12],
        [7],
        [1, 7], 
        [4, 12], 
        [7, 12], 
        [4,7,12], 
        [1, 4, 12], 
        [1, 7, 12], 
        [1, 4, 7, 12],
        [5],
        [1, 5],
        [5, 12],
        [5, 1, 12],
        [5, 7, 12],
        [1,5,7,12],
        [6],
        [1, 6],
        [5, 6],
        [5,6,12],
        [1,5,6,12],
        [1,2,3,4,5,6,7,12],
        [8],
        [1, 8],
        [8, 12],
        [9],
        [1, 9],
        [9, 12],
        [8, 9],
        [1, 8, 12],
        [1, 9, 12],
        [1, 8, 9, 12],
        [1, 2, 3, 4, 5, 6, 7, 8, 9, 12],
        [10],
        [1, 10],
        [10, 12],
        [11],
        [1, 11],
        [11, 12],
        [1, 10, 12],
        [1, 11, 12],
        [1, 10, 11, 12],
        [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],  
];

var sg_level_end = 30;
var sg_session_hits = 0;
var sg_session_misses = 0;
var sg_level_hits = 0;
var sg_level_misses = 0;
var sg_level_late = 0;
var sg_missed = false;
var sg_session_score = 0;
var sg_level_score = 0;
var sg_level_took = 0;
var sg_streak = 0;
var sg_velocity = 100;

const sg_interval_melodic = 0;
const sg_interval_harmonic = 1;

var sg_session_length = 0;
var sg_level_length = 0;
var sg_mode = sg_interval_melodic;
var sg_previous = 0;
var sg_notes = [60];                   // a maximum of four notes sounding together
var sg_note = 0;                       // which note in a sequence to play next

function sg_play_sustained_note()

{
        if (sg_note < sg_notes.length) {
                midiio.play_note(sg_notes[sg_note], -1, sg_velocity);

                sg_note++;

                if (sg_note < sg_notes.length) {
                        window.setTimeout(sg_play_sustained_note, 500);
                }
        }
}

// let the user listen to a sound
// or melodic

function sg_play()

{
        midiio.all_notes_off();

        sg_note = 0;

        // play infinitely sustained notes

        if (sg_mode == sg_interval_melodic) {
                sg_play_sustained_note();
        }
        else {
                // in harmonic mode play all the notes at once
                while (sg_note < sg_notes.length) {
                        sg_play_sustained_note();
                }
        }
}

function random_interval(min, max) { // min and max included 
  return Math.floor(Math.random() * (max - min + 1) + min);
}

function sg_new_challenge()

{
        if (sg_level_length == 0) {
              // this is the starting note, start the level timer

                sg_level_starttick = Date.now();
        }

        sg_session_length++;
        sg_level_length++;

        sg_user_score();

        sg_missed = false;

        // check if the level is over

        console.log("level length: " + sg_level_length);

        if (sg_level_length == sg_level_end) {
                // this was the last note, stop the level timer to compute the bonus 
                // if the level was executed flawlessly

                console.log("session  score is " + sg_session_score + " level " + sg_level + " level score is " + sg_level_score + "%");
                message_user("session  score: " + sg_session_score + " level score: " + sg_level_score + "%");

                console.log("now saving level");

                // save the stats, remember to convert 'took' to milliseconds from seconds and make it an integer

                sglog.save(new Date(), sg_level, sg_level_starttick, sg_level_score, sg_level_misses, sg_level_hits, 0 /* sg_level_late */, Math.floor(sg_level_took*1000))

                if (sg_level < sg_levels.length) {
                        // when you get a level perfectly you auto advance to the next
                        if (sg_level_misses == 0) {
                                // play level up melody here

                                sg_level++;
                        }
                        else {
                                // do over tune here
                        }

                        sg_new_level();
                }
                else {
                        // play session completed melody here
                        // you won't hear this one frequently...

                        sg_new_session();
                }

                return;
        }

        // pick a random note or chord or sequence
        sg_n_notes = 1;

        sg_previous = sg_notes[0];

        // pick one of the options from this level
        var choice = random_interval(0, sg_levels[sg_level].length-1);

        // and fetch the interval that belongs with that choice
        var interval = sg_levels[sg_level][choice];

        // now jump relative to the previous note, but confine the jump
        // to the range of the keyboard
        // assume we're going to go up

        if ((sg_notes[0] + interval) <= config.kbd_midi_high && (sg_notes[0] - interval) >= config.kbd_midi_low) {
                // both are possible, pick a random direction

                if (random_interval(0,1) == 0) {
                        // go down
                        interval = -interval;
                }
        }
        else {
                if (sg_notes[0] - interval >= config.kbd_midi_low) {
                        // go down
                        interval = -interval;
                }
        }

        sg_notes[0] = sg_notes[0] + interval;
}

// show the statistics

function sg_update()

{
        $("#sgprogress").html("<h1> Question " + (sg_level_length+1) + "/" + sg_level_end + " Correct " + sg_level_hits + "/" + (sg_level_hits + sg_level_misses) + " </h1>");
}

// received a midi message

function sg_message(command, note, velocity)

{
        // some keyboards don't send note-off but send note-on with a 
        // velocity of zero. This is non-standard and causes all kinds
        // of problems so we map that particular usage of note-on to
        // note-off

        if (command == 144 && velocity == 0) {
                command = 128;
        }

        // console.log("command " + command + " note " + note + " velocity " + velocity);

        switch (command) {
                case 144:
                        // mirror the loudness of the student

                        sg_velocity = velocity;
                        switch (note) {
                                case sg_previous:
                                        if (sg_notes[0] != note) {
                                                // just replay when released
                                                break;
                                        }
                                        // falls through if this is a restart
                                case sg_notes[0]:
                                        // the right note
                                        // but only count it if it wasn't done wrong first and always count the
                                        // hit if this was the first note (that one you get for free, it is always middle-c)
                                        if (sg_missed == false || (sg_level_hits == 0 && sg_level_misses == 0)) {
                                                sg_streak++;
                                                sg_level_hits++;
                                                sg_session_hits++;
                                        }
                                        
                                        sg_new_challenge();
                                        break;
                                default:
                                        // the wrong note
                                        // but only count it if it isn't the beginning of a level
                                        // because you can easily get out of sync there and then you
                                        // have to play through the whole level without being able
                                        // to get a perfect score

                                        if (sg_missed == false && (sg_notes[0] != 60 || sg_level_length > 1)) {
                                                sg_streak = 0;
                                                sg_level_misses++;
                                                sg_session_misses++;
                                        }
                                        // remember missed notes so we don't end up counting them
                                        // for every time the user retries
                                        sg_missed = true;
                                        break;
                        }
                        break;
                case 0x80:
                        // note released; play the challenge
                        sg_play();
                        break;
                default:
                        break;
        }

        sg_update();
}

function sg_new_level()

{
        // wrap level number if necessary

        if (sg_level >= sg_levels.length) {
                sg_level = 0;
        }

        if (sg_level < 0) {
                sg_level = sg_levels.length-1;
        }

        $("#sglevel").html(sg_level);

        sg_level_length = 0;
        sg_level_starttick = Date.now();

        // how we're doing in this session
        sg_level_score = 0;

        sg_level_late = 0;
        sg_level_hits = 0;
        sg_level_misses = 0;

        // load the level and show the statistics

        sg_draw_stats([]);      // assume we have no data

        sglog.load(sg_level);
      
        sglog.start();
 
        // show initial progress so far (from past sessions)
        // unfortunately due to the async nature of the database
        // load this may well be just a blank. 

        sg_update();

        // always start a new session on middle-c to give the user
        // a chance to start properly

        sg_notes[0] = 60;

        // play new level start warning sound here

        sg_play();
}

function sg_new_session()

{
        sg_session_length = 0;
        sg_level = 0;

        sg_session_hits = 0;
        sg_session_misses = 0;
        sg_session_score = 0;

        sg_new_level();
}


function sg_intervals_melodic()

{
        sg_mode = sg_interval_melodic;

        sg_new_session();
}

function sg_intervals_harmonic()

{
        sg_mode = sg_interval_harmonic;
        
        sg_new_session();
}


function sg_start()

{
        sg_intervals_melodic();     
}

function sg_prev()

{
        sg_level--;
        sg_new_level();
}

function sg_next()

{
        sg_level++;
        sg_new_level();
}


