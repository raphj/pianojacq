
// this file will contain all the unit tests, it is simply
// activated from a link in the 'settings' page for now, 
// which ensures that all of the libraries and other 
// requirements have been loaded and gives the complete
// application environment to run tests in. Each of the
// javascript modules contains its own set of tests which
// this little file will call upon.
//

var unittests_failed = 0;
var unittests_ok = 0;

function ensure(desc, fn) {
    try {
      fn();
      console.log('\x1b[32m%s\x1b[0m', '\u2714 ' + desc);
      unittests_ok++;
    } catch (error) {
      console.log('\n\x1b[31m%s\x1b[0m', '\u2718 ' + desc);
      console.error(error);
      unittests_failed++;	    
    }
}

// add an 'equals' function to arrays to make it easy to 
// compare two arrays for equality, something that we 
// will be doing a lot in the various unit tests

// lifted from:
// https://stackoverflow.com/questions/7837456/how-to-compare-arrays-in-javascript

if (Array.prototype.equals) {
        console.warn("Overriding existing Array.prototype.equals. Possible causes: New API defines the method, there's a framework conflict or you've got double inclusions in your code.");
}

// attach the .equals method to Array's prototype to call it on any array
Array.prototype.equals = function (array) {
        // if the other array is a falsy value, return
        if (!array) {
                return false;
        }

        // compare lengths - can save a lot of time
        if (this.length != array.length) {
                return false;
        }

        for (var i = 0, l=this.length; i < l; i++) {
                // Check if we have nested arrays
                if (this[i] instanceof Array && array[i] instanceof Array) {
                        // recurse into the nested arrays
                        if (!this[i].equals(array[i])) {
                                return false;
                        }
                }
                else if (this[i] != array[i]) {
                        // Warning - two different object instances will never be equal: {x:20} != {x:20}
                        return false;
                }
        }

        return true;
};
// Hide method from for-in loops
Object.defineProperty(Array.prototype, "equals", {enumerable: false});

// end of stackoverflow copied code

function assert(is_true) {
        if (!is_true) {
                throw new Error();
        }
}

console.log('Unit Tests');

function run_unittest_tests()

{
        // test the tests themselves	

        ensure('this should be true', function() { assert(1 == 1); });
        ensure('this should be false',function() { assert(1 != 1); });

        // test the array equals function	

        ensure('unequal arrays 1',function() { assert([1, 2, [3, 4]].equals([1, 2, [3, 2]]) === false); });
        ensure('unequal arrays 2',function() { assert([1, "2,3"].equals([1, 2, 3]) === false); });
        ensure('equal arrays 1',function() { assert([1, 2, [3, 4]].equals([1, 2, [3, 4]]) === true); });
        ensure('equal arrays 2',function() { assert([1, 2, 1, 2].equals([1, 2, 1, 2]) === true); });
}

function run_unit_tests()

{
	// here we should reveal the test runner output; for now
	// we'll use the console

	// console.clear();

	// dogfood is the best food... 
	// verify the function of the test runner by running
	// two tests, one that always fails and one that succeeds

	run_unittest_tests();

        if (unittests_failed != 1) {
                debugger;
        }

	// reset the unittests counters so we get a clean report
	// from the application specific unit tests

	unittests_ok = 0;
	unittests_failed = 0;

	// and clear to the console to clean up the output
	// from the unit tests tests
	//
	// clearing the console can remove useful debugging information
	// about the tests themselves (such as syntax errors in the test
	// code) so instead we'll just add some whitespace around it

	console.log('\n\n      UNIT TESTS\n\n\n');

	// now run the tests for each section of the application

        try {
		//run_index_tests();
		run_pianoroll_tests();
		//run_score_tests();
		run_chords_tests();
		run_repertoire_tests();
	
		console.log("Unit tests success ", unittests_ok, " failed ", unittests_failed);

		console.log('\n\n    UNIT TESTS END\n\n\n');

	} catch (error) {
		console.error(error);

		// stop on the first error encountered so you don't end up
		// with a large amount of output past the point of the first
		// error.
		
	    	debugger;
	}

	// we stop processing if one or more unit tests failed to pass.
	// Unfortunately I don't see a way to call the debugger and leave console 
	// displayed so an additional mouse click will be required to switch back 
	// to the console.

	if (unittests_failed > 0) {
		debugger;
	}
}

// always run the unit tests for now

run_unit_tests();

