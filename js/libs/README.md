
Files in this directory are externally procured, they are included
here to ensure that they don't change out from under us in ways
that would destabilize the project. Periodically we should scan
the originals and compare to see if it is useful to do an update.
