//
// (C) Jacques Mattheij 2020; 2021; 2022; 2023; jacques@modularcompany.com
// All rights reserved, 
//
// This file contains everything that has to do with the tickstats
// pane, which records on a per tick basis whether there were problems
// or not. 

class Tickstats {

        // This sets up an empty array to store the tickstats or the piece in, it creates a 
        // canvas to draw on and resets the scores.

        #per_tick_stats;

        constructor ()

        {
                // this really should be a private variable but too many places use it
                this.#per_tick_stats = [];

                var tickstatsrect = document.getElementById("tickstats").getBoundingClientRect();
                $("#tickstats").attr('width', tickstatsrect.width);
                $("#tickstats").attr('height', tickstatsrect.height);
                this.tickstatscanvas = document.getElementById("tickstats").getContext("2d");

                this.blank_per_tick_stat = this.#objlist(stat_history, { errors: 0, delay: 0, good: 0});

                // TODO see if this can be made private
		this.bar_scores = [];
                // TODO see if this can be made private
		this.worst_bar = 0;
        }

        #objlist(n,o)

        {
                var r = [];

                for (var i=0;i<n;i++) {
                        r.push(o);
                }

                return r;
        }


        // reset the stats to blank (for instance, because a new file is added)

        reset()

        {
                this.#per_tick_stats = [];
                this.bar_scores = [];
                this.worst_bar = 0;
        }

        // these two are kind of ugly, they give full access to the
        // internals of per_tick_stats; used for loading/saving pieces

        get_per_tick_stats()

        {
                return this.#per_tick_stats;
        }

        set_per_tick_stats(stats) 

        {
                this.#per_tick_stats = stats;

                // should we update the per_tick_stats display here already?
                // the risk is that it will be done twice
        }

        // This scores a single tick based on the state of the notes
        // over the last 'n' plays of this particular tick. Good, delay
        // and errors are the number of times the notes were played on
        // time and good, too late but good or wrong. n is the number
        // plays, it can vary in the beginning when there aren't a lot
        // of plays yet, after that it gets capped by the caller.

        // maybe it would be good to pull the pre-amble in here as well
        // because clearly the good, delay and errors values are lifted
        // from tickstats and we may well do that here.

        // a score of '1' -> this tick was played perfect every time
        // a score of '0' -> this tick was never played good

        #compute_tick_score(good, delay, errors, n)

        {
                // it is actually quite hard to compute a 'score' on a tick that
                // will steer the user to the best possible performance, because
                // a fast played piece with lots of errors may come out of the
                // computation as 'better' than a slower version without the
                // errors because of the weight given to delayed notes. The
                // only way to really deal with this is to ignore delayed
                // notes entirely until the piece can be played perfect.

                if (errors) {
                        return 0;
                }

                return (good / n) + (delay / (n * 4));

                // old scoring functions

                // return (good / n) + (delay / (n * 2));

                // older score formulas

                // return 1 - (((errors * .8) + (delay * .2)) / n);

                // return 1 - ((errors+(delay/32)) / n);
        }

        // compute the score for a single bar; also used to compute the score for a single tick
        // this uses the last 7 playthroughs weighing the most recent ones the heaviest.

        #bar_score(piece, tickindices)

        {
	        var n = 0;
	        var good = 0;
	        var errors  = 0;
	        var delay = 0;
                var have_notes = false;

	        for (var i=0;i<tickindices.length;i++) {
			// ideally the whole config would be passed in as a parameter
			// to make the function more pure
		        if (piece.n_notes_for_hand_at_tickindex(config.hand, tickindices[i]) > 0) {
                                have_notes = true;

                                // we can't calculate the stats if there is no data

                                if (typeof(this.#per_tick_stats[tickindices[i]]) == "undefined") {
                                        continue;
                                }

                                // we have some data, accumulate the various counts 
                                // we only use the last 3 plays of these ticks, the
                                // last counts for 3, then 2, then 1 so the student
                                // gets a strong reward from a perfect playthrough

			        // where does this '7' come from?

			        for (var j=0;j<7 && j< this.#per_tick_stats[tickindices[i]].length;j++) {
				        let stats = this.#per_tick_stats[tickindices[i]][j];

				        // console.log(stats);

                                        // we don't actually use the 'good' count for anything.

                                        var f = 7-j;

				        if (stats.good) {
					        good += f;
                                                n += f;
				        }

				        if (stats.delay) {
					        delay += f;
                                                n += f;
				        }

				        if (stats.errors) {
					        errors += f;
                                                n += f;
				        }
			        }
		        }
	        }

                // avoid dividing by zero

	        if (n == 0) {
                        if (have_notes) {
                                // we have notes but none got played so this is a very bad bar
		                return 0.0;
                        }
                        else {
                                // we don't have notes so this bar is always perfect
                                return 1.0;
                        }
	        }

                // return a value between 0 and 1 to indicate how good this
                // bar was

                // ideally, not having studied a bar and playing it would give you a low 20% 
                // or so score which would then go up bit by bit as you approach perfection
                // say we have a bar of 10 ticks with 5 data points for each tick. That gives
                // 50 chances for a good note, an error or a delayed note.

                // if all of them are bad then that's 0 out of 50 or 0% score
                // if 25 are good, 10 are wrong and 15 are late that should be
                // somewhere between 50 and 100%

                console.log("   good ", good, "delay ", delay, "n ", n, "score " , (good / n) + (delay / (n * 2)));

                // further reduced the effect of delayed notes a bit to 
                // prioritize the parts that have errors in them

                return this.#compute_tick_score(good, delay, errors, n);
        }

        // compute the score for all bars and order them by increasing
        // score

        #update_bar_scores(piece)

        {
	        this.bar_scores = [];

	        for (var bar=0;bar<piece.bars.length;bar++) {
		        this.bar_scores.push({ bar: bar, score: this.#bar_score(piece, piece.bars[bar].tickindices) });
	        }

	        this.bar_scores = this.bar_scores.sort((a, b) => a.score - b.score);
        }

        // return the bar number with the worst score of all bars

        #update_worst_bar()

        {
	        if (this.bar_scores.length == 0) {
		        return 0;
	        }

	        this.worst_bar = this.bar_scores[0].bar;

                return this.worst_bar;
        }

        // compute average score across all bars as a percentage

        #update_bar_average()

        {
                var sum = 0;

                if (this.bar_scores.length == 0) {
                        return 1.0;
                }

                for (var i=0;i<this.bar_scores.length;i++) {
                        sum += this.bar_scores[i].score;
                }

                this.bar_average = Math.floor((sum / this.bar_scores.length)*10000) / 100;

                return this.bar_average;
        }

        // show the user how far they are with this piece

        #draw_progress_circle(percentage)

        {
                // show the user how far they are with this piece

                $("#completion").text(percentage + "%");

                // draw a chart to make it more visual
                // TODO get rid of elseif mess
                let circle = document.getElementById("circle");
                circle.setAttribute("stroke-dasharray", percentage+", 100");

                if (percentage > 90) {
                        circle.style.stroke = '#69B34C';
                } else if (percentage > 75) {
                        circle.style.stroke = '#ACB334';
                } else if (percentage > 50 ) {
                        circle.style.stroke = '#FAB733';
                } else if (percentage > 25 ) {
                        circle.style.stroke = '#FF8E15';
                } else if (percentage > 15 ) {
                        circle.style.stroke = '#FF4E11';
                } else {
                        circle.style.stroke = '#FF0D0D';
                }
        }

        // update the bar_scores; worst_bar and bar_average all at once
        // usually this is called somewhere near the end of a practice run
        // either to inform the user of how well they did or, alternatively,
        // to prepare for some automated step to determine if the student
        // is allowed to advance to the next practice segment
        // it also conveniently redraws the progress circle

        update_scores(piece)

        {
                this.#update_bar_scores(piece, this.#per_tick_stats);

                // console.log("bar_scores: ", bar_scores);

                // figure out which bar is the worst

                this.#update_worst_bar(this.bar_scores);

                // console.log("worst_bar is ", this.worst_bar);

                this.#update_bar_average(this.bar_scores);

	        console.log("the worst bar is ", this.worst_bar, "at ", this.bar_scores[this.worst_bar].score , " average is ", this.bar_average * 100, "%");

                this.#draw_progress_circle(this.bar_average);

	        return this.bar_average;
        }

        // #gp_fill_rect is named after a graphics package that I wrote ages ago
        // (aptly named 'gp'), it had a whole slew of 2D primitives and filled
        // rectangles was one of them. Like that I don't need to remember yet
        // another function name. The parameters should be self evident.

        #gp_fill_rect(x, y , w, h , color)

        {
                this.tickstatscanvas.fillStyle = color;
                this.tickstatscanvas.fillRect(x,y,w,h);
        }

        // draw a single tick in the per_tick_stats div by filling it with
        // a rectangle of the right color for every play up to the height
        // of the div. If there is no data for a particular slot it will 
        // be drawn in white. 

        #draw_tick(tick)

        {
                var nhistory = 180;
                // check if we are asking to draw data that we don't have
        
                if (tick >= this.#per_tick_stats.length) {
                        return;
                }

                var rect = document.getElementById("tickstats").getBoundingClientRect();

                var w = rect.width / ((last_tickindex - start_tickindex) + 1);
                var h = rect.height / nhistory;

                var x = w * (tick - start_tickindex);

                for (var i=0;i<nhistory;i++) {

                        var y = i * h;

                        var color = "white";

                        if (this.#per_tick_stats[tick].length > i) {

                                if (this.#per_tick_stats[tick][i].errors) {
                                        color = "red";
                                }
                                if (this.#per_tick_stats[tick][i].good) {
                                        color = "green";
                                }
                                if (this.#per_tick_stats[tick][i].delay) {
                                        color = "lightgreen";
                                }
                        }

                        this.#gp_fill_rect(x, y, w, h, color);      
                }
        }

        // draw the complete per_tick_stats div

        draw()

        {
                for (var i=0;i<this.#per_tick_stats.length;i++) {
                        this.#draw_tick(i);
                }
        }

        // log statistics for a single tick

        log(i, e)

        {
                // only log stats within the area that we are practicing

                if (i < start_tickindex || i > last_tickindex) {
                        return;
                }

                // add a new blank entry at the beginning

                while (this.#per_tick_stats.length <= i) {
                        // the midi file was changed or the program was changed, either way, the
                        // number of ticks is no longer what it was before, so we extend it.

                        this.#per_tick_stats[this.#per_tick_stats.length] = [... this.blank_per_tick_stat];
                } 

                // prepend the new log entry

                this.#per_tick_stats[i].unshift(e);

                // immediately show the change in the display

                this.#draw_tick(i);

                // update the global stats

                total_errors += e.errors;
                total_good   += e.good + e.delay;
                total_late   += e.delay;
        }

        // this is used while drawing the piece to determine if a note
        // should be colored 'red' or not. That serves as a warning
        // to the student that they should pay good attention to that
        // particular note. After 10 successful plays of that particular
        // tick the note will no longer be red.

        tick_had_errors(i)

        {
                if (this.#per_tick_stats.length <= i) {
                        return false;  
                }

                if (i < start_tickindex || i > last_tickindex) {
                        return false;
                }

                for (var j=0;j<10;j++) {
                        if (this.#per_tick_stats[i][j].errors) {
                                // console.log("had errors tick ", i, "index", j);
                                return true;
                        }
                }

                return false;
        }

        // tickscore is used by the 'hardchords' generator to figure out where the
        // hardest ticks are in the piece so they can be practiced separately; usually
        // these are complex chords. It just wraps bar_score in the most simple way
        // by pretending to have a bar with just one tick in it and computing the
        // score on that bar.

        tick_score(piece, tickindex)

        {
                return this.#bar_score(piece,[tickindex]);
        }

        // return all the tick scores as tuples for use in generators and
        // planners; the tickindex is stored along with the score because
        // the recipients of the return value will usually sort the array 
        // or perform some other re-ordering on it that will require 
        // re-association with the right tickindex afterwards

        tick_scores(piece) 

        {

                var scores = [];

                for (var i=0;i<this.#per_tick_stats.length;i++) {
                        scores.push({ score: this.tick_score(piece, i), tickindex: i});
                }
                return scores;
        }

        // check if we have enough data to do an autopilot run, occassionally
        // will return true randomly to force the player to go through the 
        // whole piece.

        #null_stats(stats)

        {
                if (typeof(stats) == "undefined") {
                        return true;
                }

                if ((stats.delay + stats.good + stats.errors) == 0) {
                        return true;
                }

                return false;
        }

        // determine at which tick to start a playthrough because there are
        // notes in the piece for this hand there but there is no data 
        // in the per_tick_stats.
        // This is used by planners to determine if a playthrough
        // is required (and from where) before they can be run.

        no_coverage_from(piece)

        {
                // ensure coverage

                for (var i=0;i<piece.last_tickindex;i++) {
                        if (piece.n_notes_for_hand_at_tickindex(config.hand, i) > 0) {
                                if (this._null_stats(this.#per_tick_stats[i])) {
                                        return i;
                                }
                        }
                }

                // there is coverage for all of the notes 

                return -1;
        }



}

// create a singleton for this class, it just serves as a convenient way to 
// group together these functions and we need an instance to be able to call 
// them.

tickstats = new Tickstats();

