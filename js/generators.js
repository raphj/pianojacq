//
// generators dynamically generate new midi files to practice something 
// either generated on the fly or distilled from an existing piece
//
// (C) Jacques Mattheij 2020; 2021; 2022; 2023; jacques@modularcompany.com
// All rights reserved, 
// 

// Hardchords, this section takes the current score and generates a completely
// new lesson randomizing the hardest chords and notes (the ones that have
// errors in them). This lesson will not be 'nice' to listen to, it will not
// be melodic but it will help to focus on the chords and notes that the
// student tends to get wrong frequently. 

// this section is quite experimental and may not be very useful yet but
// it is a nice indication of what could be done, generate a new midi file
// on the fly for practice purposes.

function generate_hard_chords_lesson(score)

{
        if (score == null) {
                console.log("no score!");
                console.log(score);
                return;
        }

        console.log("score.bars.length", score.bars.length);

        // if there are no bars then there is no work.

        if (score.bars.length == 0) {
                console.log("no bars");

                return;
        }

        console.log(score);

        // first we compute a score per tick to see which ticks are
        // played wrong

        var scores = tickstats.tick_scores(piece);

        scores = scores.sort((a, b) => a.score - b.score);

        console.log("scores for each tick in generate_hard_chords_lesson");

        console.log(scores);

        // scores now contains the score per tick sorted worst first 
        // no we need to find out what is actually played at those ticks

        var n = scores.length / 20;     // the five % that are the very worst

        notes = [];

        for (var i=0;i<n;i++) {
                notes.push(score.notes_at_tickindex(scores[i].tickindex));
        }

        console.log("the notes that we have picked out as problematic");

        console.log(notes);

        // now construct a json encoded midi object for this set of notes
        // where the notes for each tick are repeated randomly

        // this is what a midi object looks like:

        // {"header":
        //      {"keySignatures":[],
        //       "meta":[],
        //       "name":"Tempo Track",
        //       "ppq":192,
        //       "tempos":[{"bpm":120,"ticks":0}],
        //       "timeSignatures":[{"ticks":0,"timeSignature":[4,4],"measures":0}]
        //      },
        // "tracks":
        //     [{"channel":0,
        //       "controlChanges":{},
        //       "pitchBends":[],
        //       "instrument":{"family":"piano","name":"acoustic grand piano","number":0},
        //       "name":"New Instrument",
        //       "notes":[
        //              {"duration":0.45833333333333337,
        //               "durationTicks":176,
        //               "midi":60,
        //               "name":"C4",
        //               "ticks":11,
        //               "time":0.028645833333333332,
        //               "velocity":0.7874015748031497
        //              },
        //              MORE NOTES HERE
        //              ]
        //      }
        //      MORE TRACKS HERE
        //     ]
        // }

        midi = { "header":
                 { "keySignatures":[],
                   "meta":[],
                   "name":"Tempo Track",
                   "ppq":192,
                   "tempos":[{"bpm":120,"ticks":0}],
                   "timeSignatures":[{"ticks":0,"timeSignature":[4,4],"measures":0}]
                 },
                 "tracks":
                   [{"channel":0,
                     "controlChanges":{},
                     "pitchBends":[],
                     "instrument":{"family":"piano","name":"acoustic grand piano","number":0},
                     "name":"New Instrument",
                   }]
        };

        midi.tracks[0].notes = [];

        var k = 0;

        for (var i=0;i<notes.length;i++) {
                for (var j=0;j<notes[i].length;j++) {
                        midi.tracks[0].notes[k] = {
                                "duration":0.45833333333333337,
                                "durationTicks":176,
                                "midi": notes[i][j].midi,
                                "ticks": i * 192,
                                "velocity": 0.8
                        };
                        k++;
                }
        }

        return midi;
}

