
// this file governs the eartraining tab
// it presents the user with melodic and harmonic intervals
// scales and other bits that can be identified

// the output device used is the one connected to the 
// software using the settings tab

// ideally there would be nothing to configure here, just
// hit the eartraining tab and it should automatically 
// select the training most suitable for you

// The program uses a variation on the 'spaced repetition' 
// theme (see: https://gwern.net/spaced-repetition for an
// excellent review of the technique) to schedule cards
// at the moment they are most likely to be lost and to
// ensure that training time is spent on things the user
// doesn't know yet rather than endless repetition of things
// they already know.

// There are three dimensions to the 'complexity' of learning
// a particular interval, there is the distance from the
// center note, further out is usually harder, there is the
// interval itself and there are the number of choices
// that are 'hot' and available to the user for selection. 

// The software prepares a set of flashcards that exhaustively
// covers all of these and then presents them to the user 
// one at the time, responses are evaluated and drive future
// exposure of the same card with exponential back-off for
// things already learned.

// These cards can be found in the etcards.js file where
// they are stored in order of appearance. This is shadowed
// by a table called 'Cards' in the database which records
// how the user has historically done on the cards and 
// when to display a card again. This persists across
// sessions.

// Harmonic cards and melodic cards use the same cardset
// to save some memory, we fake the different sets by
// adding a large number (100,000 right now) the base 
// of the harmonic set. This is a dirty trick that will
// probably come back to haunt me.

// For instance: 

// { id: 0, et_type: 0, notes: [65, 69], choices: ['m3', 'M3' ], answer: 1 } 
// could define a card for an interval playing two melodic notes, followed by two choices for
// intervals (minor third and major third) with the correct answer being the second one.
// the first entry is the 'card id', which can be used to associate information about the
// users' performance on this particular card in the database. It also allows for 
// identifying particularly difficult ranges because not all intervals are equally hard
// to tell apart at all frequencies. Specifying the notes gives the option for playing
// them in any order (asc, desc and for chords in open positions as well).

// By building it up over time by putting more and more cards into play as the previous
// ones are learned the complexity would very gradually increase over time without the
// risk of a huge jump in complexity. That does leave a potential problem if the user
// doesn't practice ear training for a longer time and then gets back into it because
// pretty much all of the most difficult cards will have sunk before they've been 
// trained properly so maybe reduce the number of cards in play depending on how long
// ago the last training session was?

// There will have to be some kind of mechanism to tie cards to the database that
// does not depend on the sequential ID because if the card sets change then the
// DB might get out of sync! Also, it might cause cards to be selected for play
// that are no longer in the set. One way to solve this would be to populate the
// set of cards during the first run of the program but that would fix the set of
// cards forever. Another way would be to guarantee the set of cards at the 
// time of generation *always* generates cards in the same sequence and with the 
// same IDs to only add new cards at the end of the sequence. 

var et_user_cards = []; // this shadows the 'Cards' table in the db, it records how the user did on each card.

var et_user_cards_offset = 0; // offset of the current card set in the user_cards array

// new cards start off with learning set to '10', if it ever reaches zero then that card
// is considered to be learned and that can be used to check how much of the total has 
// been mastered. 

// next: next timestamp this card should be exposed
// delay: the delay used between last exposure and 'next', to be doubled or halved depending on correctness of answer
// exposures: how often the card has been shown
// correct: how often the card has been answered correctly
// wrong: how often the card was answered wrongly
// learning: downcounter; when it reaches 0 the card is considered 'learned'

// the Usercards table

schemaBuilder.createTable('Cards').
        addColumn('id', lf.Type.INTEGER).
        addColumn('next', lf.Type.INTEGER).
        addColumn('delay', lf.Type.INTEGER).
        addColumn('exposures', lf.Type.INTEGER).
        addColumn('correct', lf.Type.INTEGER).
        addColumn('wrong', lf.Type.STRING).
        addColumn('learning', lf.Type.INTEGER).
        addPrimaryKey(['id']);

// First retrieve the user_cards made so far from persistent storage

function et_user_card_load(id,next,delay,exposures,correct,wrong,learning)

{
        var seconds = Math.floor((next - Date.now()) / 1000);

        console.log("loading " + id + " " + next + " (" + seconds + " seconds) " + delay + " " + learning);

        et_user_cards[id].next = next;
        et_user_cards[id].delay = delay;
        et_user_cards[id].exposures = exposures;
        et_user_cards[id].correct = correct;
        et_user_cards[id].wrong = wrong;
        et_user_cards[id].learning = learning;

        // unfortunately this can happen async and we do not know exactly when the
        // last record was processed so we update the user visible stats after  
        // every record loaded which is hugely wasteful

        et_update();
}

function et_user_cards_load()

{
        var ctbl = database.getSchema().table('Cards');

        database.select().from(ctbl).exec().then(function(results) {
                results.forEach(function(row) {
                        et_user_card_load(row.id, row.next, row.delay, row.exposures, row.correct, row.wrong, row.learning);
                });
        });
}

function et_user_card_save(i)

{
       console.log("saving card " + i);

       var ctbl = database.getSchema().table('Cards');
        
       var row = ctbl.createRow({
                'id': i,
                'next': et_user_cards[i].next,
                'delay': et_user_cards[i].delay,
                'exposures': et_user_cards[i].exposures,
                'correct': et_user_cards[i].correct,
                'wrong': et_user_cards[i].wrong,
                'learning': et_user_cards[i].learning
        });
        
        database.insertOrReplace().into(ctbl).values([row]).exec();
}

// first some default values for all cards that might be used
// melodic cards first

for (var i=0;i<et_cards.length;i++) {
        et_user_cards[i] = { next: 0, delay: 128, exposures: 0, correct: 0, wrong: 0, learning: 10 };
}

// harmonic cards are offset by 100k

for (var i=0;i<et_cards.length;i++) {
        et_user_cards[i+100000] = { next: 0, delay: 128, exposures: 0, correct: 0, wrong: 0, learning: 10 };
}

// TODO add chords (melodic, harmonic, open)  and scales sets here

// this scores the user across *all* ear training exercises
// not just the current one; this doesn't work perfectly 
// yet, for instance, cards that have been 'learned' that are
// shown again should either not be shown or the algorithm
// should take into account that cards that have a learning
// count of '0' are still part of the training. This is tricky
// because you want a student to feel that they are progressing
// towards a goal, not that the carrot is moved or that they are
// doing busy work without learning anything because you are
// repeating old material. But that too is learning. This will
// need a better plan.

function et_user_score()

{
        var total = 0;

        for (var i=et_user_cards_offset;i<et_user_cards_offset+et_cards.length;i++) {
                if (i in et_user_cards) {
                        total = total + et_user_cards[i].learning;
                }
        }

        return 100 * (1.0 - (total / (et_cards.length * 10)));
}

// Every now and then we increase the number of cards in play. This happens
// whenever there are no cards with more than 5 'learnings' left to go

// this isn't very elegant, especially because as more cards go 'in play'
// the routine gets slower and slower even if nothing changes

// other options are to expand the set based on how long ago something new
// got added?

function et_expand()

{
        var in_play = 0;

        // count the number of cards in the current set that the user
        // has already seen once before

        for (i=et_user_cards_offset;i<et_user_cards_offset+20000;i++) {
                if (i in et_user_cards) {
                        in_play++;
                }
        }

        // at least 10 cards in play

        if (in_play < 10) {
                in_play = 10;
        }

        var n_active = 0;
        for (var i=0;i<in_play;i++) {
                // found a card in the current set that isn't properly learned yet
                if (et_user_cards[i+et_user_cards_offset].learning > 5) {
                        n_active++;
                }
        }

        if (n_active > 10) {
                // don't expand the set of active cards

                return in_play;
        }

        // add new cards in batches of five to create some variety

        in_play = in_play + 5;

        if (in_play >= et_cards.length) {
                in_play = et_card.length;
        }

        console.log("Expanding, now " + in_play + " cards in play.");

        return in_play;
}

// pick any card from the next 10 that are due to be shown

function et_next_card()

{
        in_play = et_expand();

        var ready = [];

        for (var i=0;i<in_play;i++) {
                ready[ready.length] = { id: i, when: et_user_cards[i + et_user_cards_offset].next };
        }

        ready = ready.sort((a, b) => a.when - b.when);

        // ready = ready.reverse();

        console.log(ready);

        // ready now contains a sorted list, pick one at random from the first 10

        var theone = ready[Math.floor(Math.random() * 10)].id;

        var delta = (et_user_cards[theone].next - Date.now()) / 1000;

        console.log("card: " + et_cards[theone].id + " delta: " + delta + " delay: " + et_user_cards[theone].delay + " learning: " + et_user_cards[theone].learning);

        return et_cards[theone];
}

function synesthesia_slice(i, color) {   
      var canvas = document.getElementById('synesthesiacolors');
      var context = canvas.getContext('2d');

      var cx = canvas.width / 2;
      var cy = canvas.height / 2;

      context.beginPath();
      context.arc(cx, cy, 75, ((i * 180) * Math.PI / 180), (((i * 180) + 180) * Math.PI / 180), false);
      context.lineWidth = 150;
      context.strokeStyle = color;
      context.stroke();
}

function synesthesia_show(i, note)

{
        var names = ["C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"];
        var colors = ["#F5000F", "#6E0107", "#016BD9", "#283E00", "#86ED9E", "#FEB300",
                      "#8900FE", "#FEFCA7", "#730041", "#FF00CE", "#FED001", "#DED0D1" ];
        var name = names[note % 12];
        var color = colors[note % 12];

        synesthesia_slice(i, color);

        console.log("synesthesia_show " + i + " note " + note + " notename " + name + " color " + color);
}

function synesthesia_off()

{
}

var et_intervals_active = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];

var et_interval_names = [ 'P1', 'm2', 'M2', 'm3', 'M3', 'P4', 'A4', 'P5', 'm6', 'M6', 'm7', 'M7', 'P8' ];

// modes for sound output (harmonic = notes sound together; 
// melodic = notes sound one after the other).

const et_interval_melodic = 0;
const et_interval_harmonic = 1;
const et_chord_melodic = 2;
const et_chord_harmonic = 3;
const et_chord_open = 4;
const et_scale = 5;

// session performance

var et_mode;
var et_correct = 0;
var et_wrong = 0;


var et_first;                   // whether or not this is the first answer to this card
var et_note = 0;                // which note in a sequence to play next

function et_play_sustained_note()

{
        if (et_note < et_card.notes.length) {
                midiio.play_note(et_card.notes[et_note], -1, 100);

                synesthesia_show(et_note, et_card.notes[et_note]);

                et_note++;

                if (et_note < et_card.notes.length) {
                        window.setTimeout(et_play_sustained_note, 500);
                }
        }
}

// show a card and 
// let the user listen to an interval or chord, either harmonic
// or melodic

function et_play()

{
        midiio.all_notes_off();

        synesthesia_off();

        et_note = 0;

        // play infinitely sustained notes

        if (et_mode == et_interval_melodic) {
                et_play_sustained_note();
        }
        else {
                // in harmonic mode play all the notes at once
                while (et_note < et_card.notes.length) {
                        et_play_sustained_note();
                }
        }
}

// show the possible answers
// choices can be revealed or hidden based on whether or not
// they are possible answers

// for interval cards this should draw a circle

function et_show_choices()

{
        var choices = "";

        for (var i=1;i<13;i++) {

                var a = i * 30;

                var px = 300 + 200 * Math.cos((2 * 3.1415926 / 12) * (i-3));
                var py = 300 + 200 * Math.sin((2 * 3.1415926 / 12) * (i-3));

                choices = choices + " <div style = 'position: absolute; left: " + px + "px; top: " + py + "px;' class='etbuttonactive'>";

                if (et_card.choices.includes(et_interval_names[i])) {
                        choices = choices + "<a class='intervallinkactive' onclick=\"et_choice(" + et_card.choices.indexOf( et_interval_names[i] )  + ")\" href=\"javascript:void(0)\">" + et_interval_names[i] + "</a>";
                }
                else {
                        choices = choices + "<a class='intervallinkinactive' href=\"javascript:void(0)\">" + et_interval_names[i] + "</a>";
                }

                choices = choices + "</div><br />";
        }

        $("#etcard").html(choices);
}

function et_newcard()

{
        et_first = true;

        et_card = et_next_card();

        et_show_choices();
}

// show the statistics

function et_update()

{
        var percentage = et_user_score();

        percentage = percentage * 10000;

        percentage = Math.floor(percentage) / 10000;

        console.log("updating stats");

        $("#etprogress").html("This session: " + et_correct + " correct, " + et_wrong + " wrong. | Completed: " + percentage + " %.");
}


function et_choice(i)

{
        var id = et_card.id + et_user_cards_offset;

        if (i == et_card.answer) {
                if (et_first) {
                        // answered correctly on the first try
                        et_correct++;

                        et_user_cards[id].correct = et_user_cards[id].correct + 1;

                        // exponential back-off for cards that are guessed correct on the first try.

                        et_user_cards[id].delay = et_user_cards[id].delay * 2;

                        // good on the first try, reduce the learning counter
                        // so that eventually more new material can be brought in

                        if (et_user_cards[id].learning > 0) {
                                et_user_cards[id].learning = et_user_cards[id].learning - 1;
                        }
                }
                else {
                        // eventually answered correctly
                        // show the card faster, but not faster then once every 32 seconds

                        if (et_user_cards[id].delay > 32) {
                                et_user_cards[id].delay = et_user_cards[id].delay / 2;
                        }
                }

                // set the next exposure time

                et_user_cards[id].next  = Date.now() + (1000 * et_user_cards[id].delay);
                console.log("good!");
                
        }
        else {
                if (et_first) {
                        // first try, but user got it wrong
                        et_wrong++;
                        et_user_cards[id].wrong = et_user_cards[id].wrong + 1;
                }

                et_user_cards[id].learning = et_user_cards[id].learning + 1;
                console.log("wrong!");
        }

        et_user_card_save(id);

        console.log("good: " + et_correct + " wrong: " + et_wrong);

        et_update();

        et_first = false;

        // if the answer was right select a new card to play
        // otherwise we will just repeat this one until the user
        // gets it (so they actually learn something!)

        if (i == et_card.answer) {
                et_newcard();
        }

        // play the notes on the card

        et_play();
}

function et_chords()

{
}

function et_scales()

{
}

function et_new_session()

{
        // how we're doing in this session
        et_correct = 0;
        et_wrong = 0;

        // show initial progress so far (from past sessions)
        // unfortunately due to the async nature of the database
        // load this may well be just a blank. 

        et_update();

        // generate a card to start things off and play it

        et_newcard();

        et_play();
}

// mode switches, one for each entry in the ear training submenu

function et_intervals_melodic()

{
        et_user_cards_offset = 0;
        
        et_mode = et_interval_melodic;

        et_new_session();
}

function et_intervals_harmonic()

{
        et_user_cards_offset = 100000;
       
        et_mode = et_interval_harmonic;
        
        et_new_session();
}


function et_start()

{
        // load the cards that we've already seen with their actual values

        et_user_cards_load();

        // from here on we supposedly have the current state of the users ear training
        // session loaded.

        // start off with interval training in melodic mode
        // if the user wants another mode they can switch

        et_intervals_melodic();     
}

