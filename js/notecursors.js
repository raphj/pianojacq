//
// This file handles the notecursors, the little green or red bars 
// that appear whenever you press a key on the midi keyboard.
// (C) Jacques Mattheij 2020; 2021; 2022; 2023; jacques@modularcompany.com
// All rights reserved, 
// 

// show the keys the user is pressing.

// In pianobooster the noteon bar is dashed, when on a flat or sharp,
// that is a good hint because otherwise it is visually hard to see
// TODO

// yet another problem is that some notes can be present in more than
// one location; for instance Central C can be shown on the treble
// and on the bass clef, which one to show depends on which hand the
// user uses to strike the key, which we can not know for sure!

// This file is kind of ugly in the sense that it depends on a bunch of
// globals and functions from index.js and on the Piece class as well. 
// Those dependencies will need to be addressed before it can be cleaned 
// up further. TODO

class Notecursors {

        #find_hand_split(keys)

        {
                var low_right = 128;
                var high_left = 0;

                // if we are in a single handed mode then the split point shifts to the end
                // of the keyboard. E.g. if in right-hand mode, the split point is far left
                // of the board

                if (config.hand == righthand) {
                        return high_left;
                } else if (config.hand == lefthand) {
                        return low_right;
                }

                var split = 60;

                for (var i=0;i<keys.length;i++) {

                        // on every key that changes the low and high
                        // water marks we also adjust the other hand to
                        // start a handspan away if that's lower than 
                        // what is already there
                        //
                        // that makes the notes_on display a lot more
                        // quiet already but it still isn't perfect, in
                        // some circumstances, especially when there is a
                        // long chord held in one hand while the other hand
                        // makes large jumps the cursors can jump even though
                        // the other hand is immobile. This isn't all that easy
                        // to solve the way the note-on cursors are dealt with,
                        // in a way they should be tied to a specific note and
                        // as long as that note was held they should not be
                        // changed.
                        //
                        // But for now it works well enough, need to revisit
                        // this at some point to make it perfect.
                        //
                        // one way to achieve this is to not show more notes_on
                        // cursors than there are expected notes? (maybe not,
                        // the expected notes cursors are good visual feedback
                        // that a key is pressed)
                        //
                        // TODO

                        if (keys[i].voice == 1) {
                                // key is in the left
                                if (keys[i].midi > high_left) {
                                        high_left = keys[i].midi;
                                        if (keys[i].midi + 13 < low_right) {
                                                low_right = keys[i].midi + 13;
                                        }
                                }
                        }
                        if (keys[i].voice == 0) {
                                // key is in the right
                                if (keys[i].midi < low_right) {
                                        low_right = keys[i].midi;
                                        if (keys[i].midi - 13 > high_left) {
                                                high_left = keys[i].midi - 13;
                                        }
                                }
                        }

                        split = (low_right + high_left) / 2;
                }

                // cap the split on what we can reasonably display
                // between the two staves, you can go about an 
                // octave up or down from middle C

                if (split < 49) {
                        split = 49;
                }

                if (split > 71) {
                        split = 71;
                }

                // console.log("high_left = ", high_left, " low_right = ", low_right, " split = ", split);

                return split;
        }

        // helper function to figure out in which hand to
        // show this note. The expected keys array for the
        // current tick is a good indication, the midpoint
        // between the lowest note for the right and the
        // highest note for the left is the cut-off point

        #in_bass(i)

        {
                // how is this possible?
                // this seems to trigger when practicing the right hand only
                // TODO

                if (typeof(piece.notes_at_tickindex(realtime.get_current_tick())) == "undefined") {
                        // you can trigger this by selecting the fj.mid file,
                        // right hand only, then riff on the keys near middle
                        // C, it won't take long before you'll see this log 
                        // entry appear TODO

                        console.log("should not happen 983");

                        return false;
                }

                var split = this.#find_hand_split(piece.notes_at_tickindex(realtime.get_current_tick()));

                if (i < split) {
                        return true;
                }

                return false;
        }

        // we should get rid of this table, it can be computed on the fly for each hand individually
        // with a simple formula

        #notes_y = [
        //      C  C#   D  D#   E   F  F#   G  G#   A  A#   B
                0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  // 0...11
                0,  0,  0,  0,  0,  0,  0,  0,  0,536,531,526,  // 12..23
              516,511,506,501,496,486,481,476,471,466,461,456,  // 24..35
              446,441,436,431,426,416,411,406,401,396,391,386,  // 36..47
              376,371,366,361,356,346,341,336,331,326,321,316,  // 48..59
              306,301,296,291,286,276,271,266,261,256,251,246,  // 60..71
              236,231,226,221,216,206,201,196,191,186,181,176,  // 72..83
              166,161,156,151,146,136,131,126,121,116,111,106,  // 84..95
               96, 91, 86, 81, 76, 66, 61, 56, 51, 46, 41, 36,  // 96..107
               26,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  // 108
                0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
                0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
                0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0
        ];

        // if a note is present in the expected set for a particular hand
        // then we should definitely show it at that position.
        // TODO

        show_note_cursors(notes_active)

        {
                var n = 0;
                var o = 0;

                // notes_active should get called with a list of active notes, and a list of expected notes
                // not with an array of bits.

                if (config.notecursors) {
                        for (var i=0;i<128;i++) {
                                if (notes_active[i]) {

                                        // depending on whether we show the cursor in the
                                        // treble or bass part we will offset the position

                                        // the cut-off is the halfway mark between the
                                        // highest note in the left hand and the lowest note
                                        // in the right hand 
                                        //
                                        // this is still not perfect. For instance, after
                                        // playing a single note in the left hand, a chord
                                        // in the right hand and then the *next* tick
                                        // will have only a single note in the left hand
                                        // then all notes will jump because they will be
                                        // shown as active in the left hand whereas they
                                        // are simply still held in the right.

                                        if (this.#in_bass(i)) {
                                                o = 72; // bass staff offset - -8 adjusted for original body margin now 0
                                        }
                                        else {
                                                o = -8;  // treble staff offset- -8 adjusted for original body margin now 0
                                        }

                                        var color = "green";

                                        // this is an annoying dependency on the 'Piece' class that I would
                                        // love to get rid of, and to make things worse it *also* depends
                                        // on the 'expected' function from realtime.js. And all that just to
                                        // change the color of the cursor. A much better way would be to 
                                        // set a list of expected notes from the main program and then
                                        // to check that list here. 

                                        if (!realtime.expected(i, piece.notes_at_tickindex(realtime.get_current_tick()))) {
                                                color = "red";
                                        }

                                        $("#noteon_" + n).css({ top: (o+this.#notes_y[i]) + 'px',
                                                        background: color
                                        });
                                        $("#noteon_" + n).show();

                                        n++;
                                }
                        }
                }

                // hide all noteon_X cursors that we are not currently using

                for (;n<10;n++) {
                        $("#noteon_" + n).hide();
                }
        }

        constructor()

        {
                // turn off the note cursors on startup

                this.show_note_cursors([]);
        }
}

const notecursors = new Notecursors();
