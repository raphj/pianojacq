//
// (C) Jacques Mattheij 2020; 2021; 2022; 2023; 2024; jacques@modularcompany.com
// All rights reserved, 
// 

// The Soundguesses table holds an entry for each practice
// of a level in the soundguesser tab

// this drives the soundguesser database log and shows graphs

class Sglog {

        #sg_log;
        #bound_resultfunc;
        #saved;

        constructor() 

        {
                schemaBuilder.createTable('Sglog').
                    addColumn('id', lf.Type.INTEGER).
                    addColumn('when', lf.Type.DATE_TIME).
                    addColumn('level', lf.Type.INTEGER).
                    addColumn('starttick', lf.Type.INTEGER).
                    addColumn('points', lf.Type.INTEGER).
                    addColumn('errors', lf.Type.INTEGER).
                    addColumn('good', lf.Type.INTEGER).
                    addColumn('late', lf.Type.INTEGER).
                    addColumn('took', lf.Type.INTEGER).
                    addPrimaryKey(['id'], true).
                    addIndex('idxsg', ['level', 'starttick'], false, lf.Order.DESC);

                this.#sg_log = [];

                this.#saved = false;
        }

        // called at the beginning of a new level to recall

        start()

        {
                this.#saved = false;
        }

        // load all of the log entries for this file/segment/hand and show them in the practice log pane

        load(level)

        {
                console.log("sg_log retrieve: ", level);

                if (!db_connected) {
                        return;
                }

                var sglog = database.getSchema().table('Sglog');

                var resultfunc = function(results) {
                        // console.log(results);

                        // we zero the list here in case we get called twice in one 
                        // tick, that way one of the two will end up visible instead of
                        // a double helping.

                        this.#sg_log = [];

                        // this is *super* ugly, both the inner *and* the outer function
                        // need to be bound to 'this' separately. This totally defeats
                        // the advantage that forEach gives you

                        var rowfunc = function(row) {
                                console.log("row: ", row);

                                this.#sg_log.push(row);
                        };             

                        var bound_rowfunc = rowfunc.bind(this);           

                        results.forEach(bound_rowfunc);

                        // we now have the results so show them

                        this.#show();
                };

                this.#bound_resultfunc = resultfunc.bind(this);

                database.select().from(sglog).where(lf.op.and.apply(null, [sglog.level.eq(level)])).orderBy(sglog.starttick, lf.Order.DESC).exec().then(this.#bound_resultfunc);
        } 

        // this function should move out of the persistence layer; it is a display function.
        // it is here because of the silly callback mechanism that lovefield uses which 
        // means you can't know when the data has loaded unless you're in that callback
        // show the sglog entries for similar sessions

        // also, the display is far from ideal, what you'd really like to see is how
        // your practice 'ranks' against all of the checks for this level in the past
        // with a fair penalty applied for errors. 

        #show()

        {
                if (this.#sg_log.length > 0) {
                        sg_draw_stats(this.#sg_log);
                }
        }

        // save the current level when it has finished.

        save(when, level, starttick, points, errors, good, late, took)

        {
                // don't log the same practice session twice.

                if (this.#saved) {
                        return;
                }

                console.log("saving: " + when + " " + level + " " + starttick + " " + points + " " + errors + " " + good + " " + late + " " + took);

                this.#saved = true;

                // add the entry to the db.

                var sglog = database.getSchema().table('Sglog');

                console.log("making row");

                var row = sglog.createRow({
                        'id': 0,
                        'when': when,
                        'level': level,
                        'starttick': starttick,
                        'points': points,
                        'errors': errors,
                        'good': good,
                        'late': late,
                        'took': took
                });

                console.log("done making row");

                console.log(row);

                console.log("inserting");

                database.insertOrReplace().into(sglog).values([row]).exec();

                console.log("done");

        }
}

sglog = new Sglog();
