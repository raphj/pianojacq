
// Constant definitions for the music trainer in JavaScript
// (C) Jacques Mattheij 2020; 2021; 2022; 2023; jacques@modularcompany.com
// All rights reserved, 

// modes
// these should all be prefixed by 'mode_' TODO

const listening    = 1;  // playing the music without interaction with the user
const following    = 2;  // user indicates the tempo; software will wait until the user catches up
const mode_slide   = 3;  // range slider moves whenever player plays the segment 7 times in a row without errors
const automatic    = 4;	 // the range slider moves to the worst parts and prompts the user to improve those
const playalong    = 5;  // music indicates the tempo; user can play and is graded according to how well they play compared to the score
const hardchords   = 6;  // just focus on chords and notes that had errors in them; technicall not a mode!

// we use this to indicate which staff we are working for

const bass = 0;
const treble = 1;

// hands

const lefthand = 1;
const righthand = 2;
const bothhands = 3;

// This is the number of history entries for every tick that we
// have for each score. The history is used to record the users'
// performance on a particular piece. Making this longer will
// require an updated user interface.

const stat_history = 10;

const tabsettings = 0;
const tabsightreading = 1;
const tabsoundguesser = 2;
const tabeartraining = 3;
const tabtheory = 4;




