//
// (C) Jacques Mattheij 2020; 2021; 2022; 2023; jacques@modularcompany.com
// All rights reserved, 
// 

// The practices table holds an entry for each practice
// session that was performed from beginning to end. Interrupted
// or modified practices are not recorded because that would make
// comparing them across practices impossible. Each practice set
// is unique for a given section and hand of a particular midi file.

// This allows for a quick comparison with previous practices
// like the current one, and will show whether you are improving or not.

// the list of practices for the hand and section settings are shown
// in the practice log pane

class Practicelog {

        #practice_log;
        #bound_resultfunc;
        #saved;

        constructor() 

        {
                schemaBuilder.createTable('Practices').
                    addColumn('id', lf.Type.INTEGER).
                    addColumn('when', lf.Type.DATE_TIME).
                    addColumn('filename', lf.Type.STRING).
                    addColumn('starttick', lf.Type.INTEGER).
                    addColumn('hand', lf.Type.INTEGER).
                    addColumn('endtick', lf.Type.INTEGER).
                    addColumn('errors', lf.Type.INTEGER).
                    addColumn('good', lf.Type.INTEGER).
                    addColumn('late', lf.Type.INTEGER).
                    addColumn('took', lf.Type.INTEGER).
                    addColumn('speed', lf.Type.INTEGER).
                    addPrimaryKey(['id'], true).
                    addIndex('idxpractice', ['filename', 'starttick', 'endtick', 'hand'], false, lf.Order.DESC);

                // practicelog holds all the stats recorded for the current piece from the current starttick to the current endtick

                this.#practice_log = [];

                this.#saved = false;
        }

        // called at the beginning of a new practice

        start()

        {
                this.#saved = false;
        }

        // load all of the log entries for this file/segment/hand and show them in the practice log pane

        load(filename, starttick, endtick, practice_hand)

        {
                console.log("practice_log retrieve: ", filename, starttick, endtick, practice_hand);

                if (!db_connected) {
                        return;
                }

                var practices = database.getSchema().table('Practices');

                // AF please have a look at the next 20 lines or so to see if you think this is the
                // appropriate way to do this or if there is a better, more concise or more elegant way

                var resultfunc = function(results) {
                        // console.log(results);

                        // we zero the list here in case we get called twice in one 
                        // tick, that way one of the two will end up visible instead of
                        // a double helping.

                        this.#practice_log = [];

                        // this is *super* ugly, both the inner *and* the outer function
                        // need to be bound to 'this' separately. This totally defeats
                        // the advantage that forEach gives you

                        var rowfunc = function(row) {
                                // console.log("row: ", row);

                                this.#practice_log.push(row);
                        };             

                        var bound_rowfunc = rowfunc.bind(this);           

                        results.forEach(bound_rowfunc);

                        this.#show();
                };

                this.#bound_resultfunc = resultfunc.bind(this);

                database.select().from(practices).where(lf.op.and.apply(null, [practices.filename.eq(filename), practices.starttick.eq(starttick), practices.endtick.eq(endtick), practices.good.gt(0), practices.hand.eq(practice_hand)])).orderBy(practices.errors, lf.Order.ASC).orderBy(practices.late, lf.Order.ASC).orderBy(practices.took, lf.Order.ASC).exec().then(this.#bound_resultfunc);
        } 

        #format_date(d)

        {
                var year = d.getYear() + 1900;

                var month = d.getMonth()+1;

                var day = d.getDate();

                // Hours part from the timestamp
                var hours = d.getHours();
                // Minutes part from the timestamp
                var minutes = "0" + d.getMinutes();
                // Seconds part from the timestamp
                var seconds = "0" + d.getSeconds();

                // Will display time in 10:30:23 format
                var formatted_time = year + "/" + month + "/" + day + " " + hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);

                return formatted_time;
        }

        // turn a numerical version of practice_hand into a textual one for display

        #hand_desc(v)

        {
                return ['left', 'right', 'both'][v-1];
        }

        // compute a ratio based on the number of successful notes

        #ratio(total, errors)

        {
                return Math.floor(10000 * (1 - (errors / total))) / 100;
        }

        // this function should move out of the persistence layer; it is a display function.
        // it is here because of the silly callback mechanism that lovefield uses which 
        // means you can't know when the data has loaded unless you're in that callback
        // show the practice log entries for similar sessions

        // also, the display is far from ideal, what you'd really like to see is how
        // your practice 'ranks' against all of the practices of this piece in the past
        // with a fair penalty applied for errors. 

        #show()

        {
                // console.log(practice_log.map(logentry => 'file: ' + logentry.filename + '<br/>').join(''));

                var rank = 1;
                var highest = 0;

                this.#practice_log.map(l => highest = Math.max(highest, l.id));

                $("#practicelog").html(
                        this.#practice_log.map(l => 
                                (l.id == highest ? "<b>" : "") + rank++ + " " + this.#format_date(l.when) + ' ' + l.filename + "[" + l.starttick + "," + l.endtick + "," + this.#hand_desc(l.hand) + "] (" + l.good + " out of " + (l.good + l.errors) + ") (" + l.errors + " wrong " + this.#ratio(l.good+l.errors,l.errors) + "%) (" + l.late + " late) (took " + l.took + ") tempo " + l.speed + " %<br/>" + (l.id == highest ? "</b>" : "")).join('')
               );
        }

        // save the current practice when it has finished.

        save(filename, when, starttick, endtick, errors, good, late, took, speed, hand)

        {
                // don't ever log the same practice session twice.

                if (this.#saved) {
                        return;
                }

                this.#saved = true;

                // add the entry to the db.

                var practices = database.getSchema().table('Practices');

                var row = practices.createRow({
                        'id': 0,
                        'when': when,
                        'filename': filename,
                        'starttick': starttick,
                        'endtick': endtick,
                        'hand': hand,
                        'errors': errors,
                        'good': good,
                        'late': late,
                        'took': took,
                        'speed': speed
                });

                database.insertOrReplace().into(practices).values([row]).exec();
        }
}

practicelog = new Practicelog();
