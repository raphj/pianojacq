
// chrome only wakelock to prevent screensaver during practice 
// TODO: wakelock should only be called at the start of the practice and released once the practice is paused. 

var wakeLock = null;

function acquireWakelock() {
        if (!('wakeLock' in navigator)) {
                return; // unsupported
        }
        if (wakeLock == null) {
                // we should only take the lock if we don't already have it
                // if the lock is released, either internally or by the user
                // changing the tab we should reset the global lock
                navigator.wakeLock.request('screen').then(function(wl) {
                        wakeLock = wl;
                        wakeLock.onrelease = function() {
                                wakeLock = null;
                        };
                        console.log("acquired wakelock:", wakeLock);

                });                
        }
}

function releaseWakelock() {
        if (wakeLock !== null) {
                wakeLock.release();
        }
}

// if the use changes tabs then we will lose the wakelock, whenever they return we
// will want to reacquire

document.addEventListener('visibilitychange', function() {
        if (document.visibilityState === 'visible') {
                acquireWakelock();
        }
});

acquireWakelock();
