//
// (C) Jacques Mattheij 2020; 2021; 2022; 2023; jacques@modularcompany.com
// All rights reserved, 
// 
// The global variables
// there should be as few as possible.

// the current piece; this is the contents of the midi file

var piece_midi = null;
var piece = null;

// The 'mode' variable describes the overall mode of operation of
// the program, it makes the highest level changes to the interaction
// between the user and the software. 

var slide_grade = 3;            // slide grade 'normal', this can be overruled by the users choice

var scrolling = 0;  // positive when the scrollbar was just active, used to suppress midi output for 'scrolling' ticks (countdown)

// The width of the canvases is chosen in such a way that a canvas can hold *at least*
// one complete bar. If more can be squeezed in we do so. 

const canvas_width = 4000;      // width of one canvas section of the piece 

// each canvas that we are displaying has a position and a width

// TODO the number of canvases should be dynamically determined, preferably based on 
// the length of the piece but a config preset or some other mechanism would be ok 
// as well.

var piece_canvases = [
        { pos: 0, width: 0, canvas: "vexsvg_00" },
        { pos: 0, width: 0, canvas: "vexsvg_01" },
        { pos: 0, width: 0, canvas: "vexsvg_02" },
        { pos: 0, width: 0, canvas: "vexsvg_03" },
        { pos: 0, width: 0, canvas: "vexsvg_04" },
        { pos: 0, width: 0, canvas: "vexsvg_05" },
        { pos: 0, width: 0, canvas: "vexsvg_06" },
        { pos: 0, width: 0, canvas: "vexsvg_07" },
        { pos: 0, width: 0, canvas: "vexsvg_08" },
        { pos: 0, width: 0, canvas: "vexsvg_09" },
        { pos: 0, width: 0, canvas: "vexsvg_10" },
        { pos: 0, width: 0, canvas: "vexsvg_11" },
        { pos: 0, width: 0, canvas: "vexsvg_12" },
        { pos: 0, width: 0, canvas: "vexsvg_13" },
        { pos: 0, width: 0, canvas: "vexsvg_14" },
        { pos: 0, width: 0, canvas: "vexsvg_15" },
        { pos: 0, width: 0, canvas: "vexsvg_16" },
        { pos: 0, width: 0, canvas: "vexsvg_17" },
        { pos: 0, width: 0, canvas: "vexsvg_18" },
        { pos: 0, width: 0, canvas: "vexsvg_19" },
        { pos: 0, width: 0, canvas: "vexsvg_20" },
];

// these should probably move to 'piece.js'
// keysignature and scale are pulled from the midi header
// and are used to draw the clef and the key signature on 
// the left hand side of the visual representation of the piece

// this borks on pieces with multiple key signatures

var keysig = "C";
var scale = "major";

// 3/4 time signature means -> 3 quarter notes per bar (or 'measure')
// 6/8 time signature means -> 6 eighth notes per bar
// so (how many notes)/(of what duration) per bar

// this borks on pieces with multiple time signatures

var timesig = "4/4";
var timesig_num = 4;
var timesig_type = 4;

// start and last are the section that is currently being practiced

var start_tickindex = 0;                // we rewind to this tickindex
var last_tickindex = 1000000;           // the last tick we play

// the default tempo multiplier is also set here, the displaytext in index.html
// above the slider should reference this value upon initialization
// this is a bit annoying, it would be nicer if we chould just change it 
// here and then the displaytext would automatically update but calling
// 'speedchange' doesn't seem to have the desired effect. TODO

var tempo_multiplier = 0.8;

var total_errors = 0;
var total_good = 0;
var total_late = 0;
var start_time = 0;
var end_time = 0;

// total_waiting tracks the total time in the waiting state
// in milliseconds.
// this is used at the end of a practice run to determine
// the speed of the player.

var total_waiting = 0;

// cumulative time practiced

var total_practiced = 0;

// which of the main tabs is currently active (this effectively switches applications)
// the application always starts up in sightreading mode to confuse long time users
// the least

var current_tab = tabsightreading;

