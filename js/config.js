//
// (C) Jacques Mattheij 2020; 2021; 2022; 2023; jacques@modularcompany.com
// All rights reserved, 
//
// This deals with the configuration or state data 

// The configuration table holds all of the config
// data that is not specific to a particular piece. 
// it allows for recovery from a page reload to get
// you back to the same midi device and practice mode.
// The filename is used to reload the file you were
// practing with before the refresh or reload of 
// the page. It would probably be better to store 
// the 'hand' information with the scores but that
// makes it harder to do things like playing through
// a number of songs with the right hand in a row. 

class Config {
        constructor() 

        {
                schemaBuilder.createTable('Config').
                addColumn('id', lf.Type.INTEGER).
                addColumn('midiin', lf.Type.INTEGER).       // the index of the midi device used for sound input
                addColumn('midiout', lf.Type.INTEGER).      // the index of the midi device used for sound generation
                addColumn('mode', lf.Type.INTEGER).         // the mode (listening, following)
                addColumn('hand', lf.Type.INTEGER).         // the hand that is active
                addColumn('filename', lf.Type.STRING).      // the name of the file we are currently practicing
                addColumn('midilow', lf.Type.INTEGER).      // the lowest value the keyboard can generate
                addColumn('midihigh', lf.Type.INTEGER).     // the highest midi value the keyboard can generate
                addColumn('diff', lf.Type.INTEGER).         // the difficulty factor, 1 = just single notes, two is two notes at the same time, three is triads, four = everything
                addColumn('karma', lf.Type.INTEGER).	// the nummber of karma points the user has
                addColumn('echoinout', lf.Type.INTEGER).    // whether midi input should be echoed to the output port
                addColumn('keepup',lf.Type.INTEGER).        // whether to try to keep up with the pianist or enforce tempo
                addColumn('gradespeed',lf.Type.INTEGER).    // whether to grade on speed as well (as accuracy, which you are always graded on) during sliding practice
                addColumn('notenames',lf.Type.INTEGER).     // whether to show note names or not
                addColumn('notecursors',lf.Type.INTEGER).   // whether to show note cursors or not

                addPrimaryKey(['id']);

                // give all of the config variables a reasonable default
                // these could be handled by setters, which then would trigger
                // autosave on change. That would get rid of the final 'save' 
                // in index.js and it would make this Class that much more
                // independent.

                // some of these should be changed to follow the fieldnames in the record
                // TODO

                this.midiin = 1;
                this.midiout = 1;
                this.echoinout = 1;
                this.mode = following;
                this.hand = bothhands;
                this.diff = 4;  // four note maximum by default
                this.keepup = 1;
                this.filename = null;
                this.gradespeed = 0;
                this.notenames = 1;
                this.notecursors = 1;

                // organ coupler simulation for automatic chording, more bass or treble from one note

                this.coupleplusplus = 0;
                this.coupleplus = 0;
                this.couplemanualup = 0;
                this.coupleminus = 0;
                this.coupleminusminus = 0;

                this.karma = 1;
                this.kbd_midi_low = 21;
                this.kbd_midi_high = 108;       // default is the whole piano range.

                // these are not yet persisted, are currently constant but really should
                // be user configurable

                // this.paper = "#e0d3af";// old manuscripts
                this.paper = "white";     // boring white
                // this.paper = "beige";     // a bit less boring than white
                this.minvelocity = 0;
        }

        save()

        {
                var configtbl = database.getSchema().table('Config');

                var row = configtbl.createRow({
                        'id': 0,
                        'midiin': this.midiin,
                        'midiout': this.midiout,
                        'mode': this.mode,
                        'hand': this.hand,
                        'diff': this.diff,
                        'filename': this.filename,
                        'midilow': this.kbd_midi_low,
                        'midihigh': this.kbd_midi_high,
		        'karma': this.karma,
                        'echoinout': this.echoinout,
                        'keepup': this.keepup,
                        'gradespeed': this.gradespeed,
                        'notenames': this.notenames,
                        'notecursors': this.notecursors
                });

                database.insertOrReplace().into(configtbl).values([row]).exec();
        }

        read_row(row)

        {
                // set all global configuration variables

                this.midiin = row.midiin;
                this.midiout = row.midiout;
                this.mode = row.mode;
                this.hand = row.hand;
                this.diff = row.diff;

                this.kbd_midi_low = row.midilow;
                this.kbd_midi_high = row.midihigh;

                this.karma = row.karma;

                this.echoinout = row.echoinout;

                this.keepup = row.keepup;

                this.gradespeed = row.gradespeed;

                if (this.gradespeed != 1 && this.gradespeed != 0) {
                        this.gradespeed = 0;
                }

                this.notenames = row.notenames;

                if (this.notenames != 1 && this.notenames != 0) {
                        this.notenames = 1;
                }

                this.notecursors = row.notecursors;

                if (this.notecursors != 1 && this.notecursors != 0) {
                        this.notecursors = 1;
                }

                this.filename = row.filename;

                config_loaded();
        }

        load()

        {
                var configtbl = database.getSchema().table('Config');

                // there really should only be one config record
                // very, very annoying: the 'this' parameter is not available
                // in the async called body of the function so we have directly
                // refer to the class instance by name. This works because there
                // is only one instance but it is yet another bit of proof of how
                // broken this whole async database thing is.

                database.select().from(configtbl).where().exec().then(function(results) {
                        results.forEach(function(row) { config.read_row(row); });
                });
        }       
}

config = new Config;
